#!/usr/bin/env bash

root=$(pwd)


if [ ! -d "./dist" ]; then
    mkdir dist
fi

cd "$root/simple"
tar -cvzf "$root/dist/apex-simple.tar.gz" --exclude=.envrc ./

cd "$root/multiple-environments"
tar -cvzf "$root/dist/apex-multiple-environments.tar.gz" --exclude=.envrc ./

# 해제확인
#tar -xvzf "$root/dist/simple.tar.gz" -C "$root/dist/test"
#tar -xvzf "$root/dist/multiple-environments.tar.gz" -C "$root/dist/test-m"
