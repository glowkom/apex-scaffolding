#!/usr/bin/env bash

load(){
    path=$1
    echo "현재 경로에 만드시려면 엔터를, 폴더를 만드시려면 폴더명을 넣으세요"
    read DIR
    if [ ! $DIR = "" ]; then
        mkdir "$DIR"
        cd "$DIR"
    else
        DIR="./"
    fi
    curl -O "https://ec.glowpick.net:14080/devtools/apex-scaffolding/dist/$1"
    tar -xvzf "$1" -C ./
    rm -f $1;
}


echo -e "\n\n"
printf "템플릿 타입을 선택하세요.  (1/2)?\n"
echo -e "\033[32m"
echo -e "    - 1.Simple"

echo -e "\033[36m"
echo -e "    - 2.Multiple Environments"

echo -e "\033[0m\n\n"

read TYPE

case "$TYPE" in
 1) load "apex-simple.tar.gz";;
 2) load "apex-multiple-environments.tar.gz";;
 *) echo "잘못된 입력입니다.";;
esac

#if [ $NUM = "1" ]; then
#load "apex-simple.tar.gz"
#elif [ $NUM = "2" ]; then
#load "apex-multiple-environments.tar.gz"
#else
#echo "잘못된 입력입니다. 1 또는 2 중에 입력해 주세요."
#fi

exit 0