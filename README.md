## APEX 소개.
####1. apex란?


* serverless architecture
* AWS Lambda 빌드 배포 관리 툴.
* AWS Lambda에서 지원하지 않는 언어를 사용하거나 Nodejs shim등을 사용할 수 있다.
* 기능 테스트, 배포 롤백, 메트릭보기, 테일 로그, 빌드 시스템에 연결하기 등 다양한 워크 플로우 관련 툴링이 제공됩니다.

####2. apex를 이용해 할수 있는 명령.


* init : 프로젝트 생성(자동 role 생성으로 인해 사용하지 않음)

* list : 함수 목록 출력


```bash
# 테라폼 변수 출력도 지원
$ apex list --tfvars
apex_function_bar="arn:aws:lambda:us-west-2:293503197324:function:testing_bar"
apex_function_foo="arn:aws:lambda:us-west-2:293503197324:function:testing_foo"
```
    
* build : 배포를 위해 zip파일 생성
```bash
apex build foo > out.zip
```

* deploy: 배포
```bash
# 현재 디렉토리의 모든 함수 배포
apex deploy

# 지정된 디펙토리의 모든 함수 배포
apex deploy -C ~/dev/myapp

# 지정한 함수만 배포
apex deploy [name]

# auth로 지정된 모든 함수 배포
apex deploy auth*

# -test로 끝나는 모든 함수 배포
apex deploy *-test

```

* invoke : 실행 ( deploy된 함수 실행. 로컬에 변경사항이 실행되지 않음. )
```bash
# 이벤트 없이 실행.
apex invoke [name]

# 이벤트로 JSON을 전달
echo -n '{ "value": "Tobi the ferret" }' | apex invoke uppercase
{ "value": "TOBI THE FERRET" }

# 파일을 전달해 실행.
apex invoke uppercase < event.json

# 클립보드 내용을 전달해 실행.
pbpaste | apex -C path/to/project invoke auth

# phony를 이용해서 데이터를 생성한 후 여러 리퀘스트를 생성하여 함수 실행하기
$ echo -n '{ "user": "{{name}}" }' | phony | apex invoke uppercase
{"user":"DELMER MALONE"}
{"user":"JC REEVES"}
{"user":"LUNA FLETCHER"}
...

# 스트리밍으로 함수를 실행하고 로그를 출력하기
$ echo -n '{ "user": "{{name}}" }' | phony | apex invoke uppercase --logs
START RequestId: 30e826a4-a6b5-11e5-9257-c1543e9b73ac Version: $LATEST
END RequestId: 30e826a4-a6b5-11e5-9257-c1543e9b73ac
REPORT RequestId: 30e826a4-a6b5-11e5-9257-c1543e9b73ac  Duration: 0.73 ms   Billed Duration: 100 ms     Memory Size: 128 MB Max Memory Used: 10 MB
{"user":"COLTON RHODES"}
START RequestId: 30f0b23c-a6b5-11e5-a034-ad63d48ca53a Version: $LATEST
END RequestId: 30f0b23c-a6b5-11e5-a034-ad63d48ca53a
REPORT RequestId: 30f0b23c-a6b5-11e5-a034-ad63d48ca53a  Duration: 2.56 ms   Billed Duration: 100 ms     Memory Size: 128 MB Max Memory Used: 9 MB
{"user":"CAROLA BECK"}
START RequestId: 30f51e67-a6b5-11e5-8929-f53378ef0f47 Version: $LATEST
END RequestId: 30f51e67-a6b5-11e5-8929-f53378ef0f47
REPORT RequestId: 30f51e67-a6b5-11e5-8929-f53378ef0f47  Duration: 0.22 ms   Billed Duration: 100 ms     Memory Size: 128 MB Max Memory Used: 9 MB
{"user":"TOBI FERRET"}
...
```

* rollback : 이전 버전으로 되돌리기.
```bash
# 이전 버전으로 되돌리기.
apex rollback foo

# 특정 버전으로 되돌리기.
apex rollback foo 5

# 롤백 미리보기.(--dry-run)
$ apex rollback --dry-run lowercase
 
~ alias testing_lowercase
 alias: current
 version: 2
 
$ apex rollback --dry-run uppercase 1
 
~ alias testing_uppercase
 version: 1
 alias: current

```

* alias : 별칭지정.

```bash
# 모든 함수에 prod 앨리어스 지정.
apex alias prod

# api_로 시작하는 이름을 가진 모든 함수의 앨리어스를 prod로 설정
apex alias prod api_*

# 버전이 5인 모든 함수의 앨리어스를 prod로 설정
apex alias -v 5 prod

# 특정 함수의 앨리어스를 stage로 설정
apex alias stage myfunction

# 특정 함수의 버전 10의 앨리어스를 stage로 설정
apex alias -v 10 stage myfunction

# 앨리어스가 dev인 특정 함수의 앨리어스를 stage로 변경 (dev에서 stage로 승격)
apex alias stage dev myfunction
```

* delete : 삭제 -f, --force 플래그를 사용하면 강제 삭제되며, 플래그 없이 사용시 삭제 여부 확인.
```bash
# 전체 삭제
$ apex delete
The following will be deleted:
 
  - bar
  - foo
 
Are you sure? (yes/no):

# 특정 함수 삭제
apex delete -f foo bar

# auth 로 시작하는 이름의 함수만 삭제.
apex delete auth*
```


####3. apex를 이용해 설정 할수 있는 범위.


* json 파일명 분리를 통해. 다중환경에 대한 설정(개발버전/배포버전)
* project.json을 이용한 설정.
    * name : 프로젝트 이름
    * description : 프로젝트 설명
    * runtime : 함수 실행에 대한 런타임 타입
        * `java (Java 8)`
        * `python2.7 (Python 2.7)`
        * `python3.6 (Python 3.6)`
        * `nodejs4.3 (Node.js 4.3)`
        * `nodejs4.3-edge (Node.js 4.3 Edge)`
        * `nodejs6.10 (Node.js 6.10)`
        * `golang (any version)`
        * `clojure (any version)`
        * `rust-musl[^rust-runtime][^rust-linux-only] (any version)`
        * `rust-gnu[^rust-runtime][^rust-linux-only] (any version)`
    * memory : 가용메모리
    * timeout : 제한시간
    * role : function.json에 명시되지 않을경우 기본 설정.
    * profile : AWS 프로파일명.  AWS_PROFILE 환경변수 또는 --profile 옵션을 사용하지 않을때 설정 가능하다
    * defaultEnvironment : 기본 인프라 환경.
    * nameTemplate : 생성될 람다 함수명 템플릿. 기본값은 {{.Project.Name}}_{{.Function.Name}},
    함수명을 그대로 사용하려면 {{.Function.Name}}로 지정하고, functions 에 지정된 함수명으로 생성된다.
    * retainedVersion : 함수의 히스토리, 기본 10개가 저장된다.
    * vpc : 기본 VPC 설정.
    * vpc.securityGroup : 보안그룹 아이디 목록.
    * vpc.subnets : 서브넷의 ID
* function.json을 이용한 설정.
    * description : 함수설명.
    * runtime : 해당 함수실행에 대한 런타임 타입(project.json 설정과 동일)
    * handler : 함수 최초 실행을 위한 핸들러 명.
        * `nodejs`: `index.handle` (index.js file with handle exported function)
        * `python`: `handle`
        * `java`: `lambda.Main::handler`
    * memory : 가용메모리 (mb)
    * timeout : 타임아웃. 기본 `300s`
    * role : AWS Lambda role
    * environment : 지정할 환경변수.
    * retainedVersions : 함수 히스토리 기본 10개의 버전까지 저장된다.
    * vpc
    * vpc.securityGroups
    * vpc.subnets
    * kms_arn : 함수의 환경 변수를 암호화하는 데 사용되는 KMS 키의 선택적 ARN. 비어 있으면 AWS Lambda 기본 서비스 키를 사용하고 있음을 의미.
    * deadletter_arn : Dead Letter Queue (DLQ)로 지정한 Amazon SQS 대기열 또는 Amazon SNS 주제의 선택적 ARN.
    
* 함수 Hook(project.json 파일에 명시 Lint, 트랜스파일을 위한 Babel등 생명주기에 맞춰 shell command 실행 지원.)
    * `build` -> zip 파일로 함수를 빌드하기 전에 실행 (바이너리로 컴파일하거나 소스 코드의 트랜스폼에 이용)
    * `deploy` -> 함수가 배포되기 전에 실행 (테스트, lint 등에 이용)
    * `clean` -> 함수가 배포되고 난 후에 실행된다 (빌드 결과물 정리 등에 이용)

```json
// Golang지원을 위한 hook 예제(project.json)
{
    "hooks": {
        "build": "GOOS=linux GOARCH=amd64 go build -o main main.go",
        "clean": "rm -f main"
    }
}
```

####4. flag

* --dry-run : 모든 명령에서 미리 실행해 결과 확인이 가능한다.
    * `+` 리소스는 생성될 것입니다.
    * `-` 리소스는 삭제될 것입니다.
    * `~` 리소스는 업데이트 될 것입니다.
    
* -s, --set : 환경변수 설정.
```bash
# 예시
apex deploy -s LOGGLY_TOKEN=token log-collector
```
    
* -E, --env-file : 환경변수 파일 사용.
```bash
apex deploy --env-file /path/to/env.json
```

```json
// env.json
{
  "LOGGLY_TOKEN": "12314212213123"
}
```

환경변수는 project.json, function.json에서 설정 가능합니다.
```json
{
  "environment": {
    "NODE_ENV": "development"
  }
}
```

* 환경변수 적용 우선순위
    1. -s, --set 플래그
    2. -E, --env-file 플래그
    3. project.json 혹은 function.json 파일에 설정된 환경 변수 값

####5. 기타.
* 파일 무시(omitting) - 람다 패키징 과정에서 제외됨
.apexignore 파일을 프로젝트 루트나 특정 함수 디렉토리에 배치, 
gitignore 파일과 동일한 패턴을 사용하여 파일을 제외함. 
기본적으로 .apexignore, function.json 파일은 무시됨.
* 로그확인
* 성능 지표 확인
* 인프라스트럭처 관리

####6. apex를 이용하는데 있어 주의할 사항. 

* apex init으로 프로젝트를 생성하지 않는다.
* 스케폴딩으로 프로젝트를 생성하고, role, vpc 설정을 우선으로 한다.(관련된 내용은 추후 공유)
* 템플릿명은 함수명으로 사용한다.
* 기존 함수는 동기화 되지 않는다. 
기존 함수를 고칠때는 기존 환경을 만들어 deploy를 한차례 진행해 수동으로 동기화 한 후 사용해야 한다. 
같은 이름의 함수가 deploy되면 덮어씌워 진다. 
(실수로 기존의 함수를 덮을 경우는 AWS console에서 이전 버전으로 복원 가능하다.)
* 이후 생성하는 nodejs runtime 은 nodejs8.10 으로 지정한다.

---




## APEX 이용 가이드.
* ##### APEX의 init명령을 사용해 함수와 구조를 만들지 않기 위해 스케폴딩을 제공합니다.
* ##### 스케폴딩에 추가해야할 것이 있다면 아래 스케폴딩 템플릿 업데이트 항목을 참고해 업데이트 합니다






### 사용해 보기.
####1. install APEX(On macOS, Linux, or OpenBSD)
````bash
curl https://raw.githubusercontent.com/apex/apex/master/install.sh | sh
````
window 는 [바이너리를 다운로드](https://github.com/apex/apex/releases) 해 사용합니다 


이미 설치되어있다면 update command를 이용해 최신화 합니다.
````bash
apex upgrade
````

####2. 자격증명을 사용할 환경 구축(aws-cli, direnv 등)
* 환경변수 이용, 아래 변수는 필수 값 입니다.


여러개의 AWS 프로젝트가 있을경우 [direnv](https://direnv.net/)등을 사용해
환경변수를 자동으로 설정하면 좋습니다
    * `AWS_ACCESS_KEY_ID`
    * `AWS_SECRET_ACCESS_KEY`
    * `AWS_REGION`
    
* aws-cli 이용.(~/.aws에 `AWS_PROFILE`을 정의 합니다.)
    * [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)
    
    
````bash
$ aws configure --profile [name]
AWS Access Key ID [None]: [your-access-key-id]
AWS Secret Access Key [None]: [your-secret-access-key]
Default region name [None]: [your-region-name]
Default output format [None]: json
````
`AWS_PROFILE`변수를 지정하거나 이후 apex 설정에서 프로필을 지정합니다.
````bash
export AWS_PROFILE=[name]
````
    
####3. 스케폴딩 템플릿 다운로드.
*  shell 파일 다운로드 
````bash
curl -O https://ec.glowpick.net:14080/devtools/apex-scaffolding/scaffold.sh
````

* 실행
````bash
bash scaffold.sh
```` 

* 필요한 샘플 다운로드.

####4. 함수 추가 및 사용.

* functions 폴더내에 함수로 사용할 폴더 생성.
* project.json, functions/**/function.json 파일 수정 및 생성.
* 새로 만든 함수 폴더에 진입점 파일 생성.
* apex 명령을 이용. 빌드 또는 배포.

---





## 스케폴딩 템플릿 업데이트

test-ec(tomcat@172.31.50.155)인스턴스 내에 내려받아 놓음.(/app/gp-front/devtools/apex-scaffolding)
샘플 수정후에는 인스턴스내에서 pull명령으로 파일 최신화 필요함.
```bash
git clone https://glowpick@bitbucket.org/glowkom/apex-scaffolding.git
```

---





## 관련 링크
* [공식 홈페이지](http://apex.run/#aws-credentials)
* [Apex 번역글](https://airkjh.com/2017/06/29/aws-%EB%9E%8C%EB%8B%A4-%ED%95%A8%EC%88%98-%EA%B4%80%EB%A6%AC-%ED%88%B4-apex-1/)
